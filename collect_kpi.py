# SPDX-FileCopyrightText: 2023 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
# SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
#
# SPDX-License-Identifier: EUPL-1.2

from typing import OrderedDict, Tuple
from urllib.error import HTTPError
import os
import requests
from pathlib import Path
import pandas as pd

BACKEND_URL = os.environ.get("BACKEND_URL")

# VOs without GFZ see https://hifis.net/doc/helmholtz-aai/list-of-vos/
HGF_ORGANISATIONS = [
    "AWI",
    "CISPA",
    "DESY",
    "DKFZ",
    "DLR",
    "DZNE",
    "FZJ",
    "GEOMAR",
    "GFZ",
    "GSI",
    "hereon",
    "HMGU",
    "HZB",
    "HZDR",
    "HZI",
    "KIT",
    "MDC",
    "UFZ",
]

stats_file = "stats/data.csv"


def get(
    endpoint: str, headers: dict = {}, expected_status_code: int = 200
) -> requests.Response:
    """Perform GET request to backend

    Requires `BACKEND_URL` to be set and pointing to `https://<host>/api/v1`

    Parameters
    ----------
    endpoint : str
        The API endpoint listed in swagger
    headers : dict, optional
        Headers to be sent in the request, by default {}
    expected_status_code : int, optional
        Expected status code, by default 200

    Returns
    -------
    _type_
        _description_

    Raises
    ------
    HTTPError
        _description_
    """
    res = requests.get(
        url=f"{BACKEND_URL}/{endpoint}",
        headers=headers,
    )
    if res.status_code != expected_status_code:
        raise HTTPError(res.status_code, res.reason)
    return res


def get_homepage_counts() -> dict:
    """Return homepage statistics

    Returns
    -------
    dict with the following keys
        * "software_cnt": number of published software entries
        * "project_cnt": number of projects
        * "contributor_cnt": total number of contributors
        * "software_mention_cnt": number of mentions
    """
    res = get("rpc/homepage_counts")
    return res.json()


def get_count(endpoint: str) -> int:
    """Returns the count of rows for the endpoint

    Returns count of rows for an endpoint by returning the Content-Range.

    Parameters
    ----------
    endpoint : str
        API endpoint as listed in Swagger

    Returns
    -------
    int
        The count
    """
    res = get(
        endpoint=endpoint,
        headers={
            "Range-Unit": "items",
            "Prefer": "count=exact",
        },
    )
    return int(res.headers["Content-Range"].split("/")[1])


def get_organisation_user_counts() -> Tuple[int, int, int]:
    """Return user counts

    Returns
    -------
    accounts_gfz, accounts_hgf, accounts_total
    """
    HGF_ORGANISATIONS_WITHOUT_GFZ = HGF_ORGANISATIONS.copy()
    HGF_ORGANISATIONS_WITHOUT_GFZ.pop(HGF_ORGANISATIONS_WITHOUT_GFZ.index("GFZ"))
    response = get(endpoint="user_count_per_home_organisation").json()
    accounts_gfz = 0
    accounts_hgf = 0
    accounts_other = 0
    for row in response:
        if row["home_organisation"] == "GFZ":
            accounts_gfz = row["count"]
        elif row["home_organisation"] in HGF_ORGANISATIONS_WITHOUT_GFZ:
            accounts_hgf += row["count"]
        else:
            accounts_other += row["count"]
    accounts_hgf += accounts_gfz
    accounts_total = accounts_hgf + accounts_other
    return accounts_gfz, accounts_hgf, accounts_total


if __name__ == "__main__":
    kpi_dtypes = OrderedDict(
        {
            "date": pd.Timestamp,
            "accounts_gfz": "Int32",
            "accounts_hgf": "Int32",
            "accounts_total": "Int32",
            "new_accounts": "Int32",
            "deprovisioned_accounts": "Int32",
            "software": "Int32",
            "projects": "Int32",
            "organisations_hgf": "Int32",
            "organisations_total": "Int32",
            "software_mention_cnt": "Int32",
            "contributor_cnt": "Int32",
            **OrderedDict({"sw_" + org.lower(): "Int32" for org in HGF_ORGANISATIONS}),
            "comment": "string",
        }
    )

    if Path(stats_file).exists():
        kpi_dtypes.pop("date")
        kpis = pd.read_csv(
            stats_file,
            index_col=0,
            skiprows=3,
            names=kpi_dtypes,
            dtype=kpi_dtypes,
        )
    else:
        kpis = pd.DataFrame(kpi_dtypes).set_index("date")

    # Adds new columns after specified reference column
    # Uncomment to run locally to update csv
    # ref_position = kpis.columns.get_loc("organisations_total")
    # kpis.insert(ref_position + 1, "software_mention_cnt", [None]*kpis.shape[0])
    # kpis.insert(ref_position + 2, "contributor_cnt", [None]*kpis.shape[0])

    new_kpis = OrderedDict({})

    # KPIs

    homepage_counts = get_homepage_counts()
    accounts_gfz, accounts_hgf, accounts_total = get_organisation_user_counts()

    # KPI#1 - Number of user accounts at GFZ
    new_kpis["accounts_gfz"] = accounts_gfz

    # KPI#2 - Number of Helmholtz user accounts (inkl. GFZ)
    new_kpis["accounts_hgf"] = accounts_hgf

    # KPI#3 - Number of user accounts in total (incl. external users)
    new_kpis["accounts_total"] = accounts_total

    # KPI#4 - Number of software (published only)
    new_kpis["software"] = homepage_counts["software_cnt"]

    # KPI#5 - Number of projects
    new_kpis["projects"] = homepage_counts["project_cnt"]

    # KPI#6 - Number of organisations total
    new_kpis["organisations_total"] = homepage_counts["organisation_cnt"]

    # KPI#7 - Number of organisations (Helmholtz only)
    new_kpis["organisations_hgf"] = get_count(
        "organisation?is_tenant=eq.true&parent=is.null"
    )

    if kpis.size > 0:
        last_timestamp = kpis.index[-1]
        # KPI#8 - Total number of new user accounts since laste date
        new_kpis["new_accounts"] = get(
            f"rpc/new_accounts_count_since_timestamp?timestmp={last_timestamp}"
        ).json()

        # KPI#9 - Total number of deprovisioned user accounts since last date
        new_kpis["deprovisioned_accounts"] = (
            new_kpis["accounts_total"]
            - kpis["accounts_total"].get(last_timestamp)
            - new_kpis["new_accounts"]
        )

    # KPI#10 Number of mentions
    new_kpis["software_mention_cnt"] = homepage_counts["software_mention_cnt"]

    # KPI#11 Contributor count
    new_kpis["contributor_cnt"] = homepage_counts["contributor_cnt"]

    # Get software count by centre
    software_count_by_organisation_id = {
        org["organisation"]: org["software_cnt"]
        for org in get("rpc/software_count_by_organisation").json()
    }
    org_id_to_slug = {
        org["id"]: org["slug"]
        for org in get(
            "organisation?is_tenant=eq.true&parent=is.null&select=id,slug"
        ).json()
    }
    sw_count_by_organisation = {
        org_id_to_slug[org_id]: software_count_by_organisation_id.get(org_id, 0)
        for org_id in org_id_to_slug.keys()
    }
    for centre in HGF_ORGANISATIONS:
        new_kpis["sw_" + centre.lower()] = int(sw_count_by_organisation[centre.lower()])

    # Add empty column for comments, needs to be populated manually
    new_kpis["comment"] = ""

    # Append to existing DataFrame
    new_row = pd.DataFrame(new_kpis, index=[pd.Timestamp.now().isoformat()])
    kpis = pd.concat([kpis, new_row])

    # Property name, plot, unit
    header_props = [
        ["date-time UTC", "plot", "unit"],
        ["Accounts at GFZ", 1, "Number"],
        ["Helmholtz accounts", 1, "Number"],
        ["Accounts in total", 1, "Number"],
        ["New accounts", 1, "Number"],
        ["Deprovisioned accounts", 1, "Number"],
        ["Software entries", 1, "Number"],
        ["Projects", 1, "Number"],
        ["Helmholtz organisations", 0, "Number"],
        ["Organisations in total", 1, "Number"],
        ["Software mentions", 1, "Number"],
        ["Contributor count", 1, "Number"],
        *[["sw_" + centre.lower(), 0, "Number"] for centre in HGF_ORGANISATIONS],
        ["Comment", 0, "N/A"],
    ]

    header_rows = ["#"] * 3
    for i in range(3):
        header_rows[i] = "#" + ",".join([str(prop[i]) for prop in header_props])
    header = "\n".join(header_rows) + "\n"

    with open(stats_file, "w") as fout:
        fout.write(header)
        kpis.to_csv(fout, header=False)
