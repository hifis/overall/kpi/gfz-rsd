-i https://pypi.org/simple
certifi==2022.12.7 ; python_version >= '3.6'
charset-normalizer==3.0.1
idna==3.4 ; python_version >= '3.5'
numpy==1.24.2 ; python_version >= '3.10'
pandas==1.5.3
pyjwt==2.6.0
python-dateutil==2.8.2 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
pytz==2022.7.1
requests==2.28.2
six==1.16.0 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
urllib3==1.26.14 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5'
