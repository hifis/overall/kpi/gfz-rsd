<!--
SPDX-FileCopyrightText: 2023 Christian Meeßen (GF) <christian.meessen@gfz-potsdam.de
SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences

SPDX-License-Identifier: CC-BY-4.0
-->

# Service Usage

* GFZ-RSD

## Plotting

* Plotting is be performed in the [Plotting project](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci).

## Data

| Data                    | Unit            | Weighting, Comment   |
|-------------------------|-----------------|----------------------|
| Accounts at GFZ         | Quantity/Number | not yet considered   |
| Helmholtz accounts      | Quantity/Number | not yet considered   |
| Accounts in total       | Quantity/Number | 25%                  |
| New accounts            | Quantity/Number | 0, just calculation  |
| Deprovisioned accounts  | Quantity/Number | 0. just calcuation   |
| Software entries        | Quantity/Number | 25%                  |
| Projects                | Quantity/Number | 25%                  |
| Helmholtz organisations | Quantity/Number | 0, included in total |
| Organisations in total  | Quantity/Number | 25%                  |
| sw_\<centre\>           | Quantity/Number | sw count per centre  |
| Comment                 | N/A             |                      |

## Schedule

* daily

## Workflow

This repository uses scheduled CI/CD pipelines to update itself. For the pipeline to work, a Docker image needs to be built.

### Docker image

If the Docker image is not present, or if there were changes to the Python dependencies, it needs to be built first.
To build the image, trigger a new pipeline in the web interface and run the `build_kpi_image` job.

To update the dependencies in the docker container, update the `reuirements.txt` first:

```bash
pipenv update
pipenv requirements > requirements.txt
```

## License hint

This project follows the REUSE specification.

* Code is licensed EUPL-1.2
* Texts and images are licensed CC-BY-4.0
* Trivial files are licensed CC0-1.0
